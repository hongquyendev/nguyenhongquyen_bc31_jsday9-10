/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập
 * @param {*} value là giá trị người dùng nhập
 * @param {*} selectorError  hiển thị lỗi cho giá trị đó
 * @param {*} name  tên thuộc tính bị lỗi khi hiển thị
 * @param {*} return trả về giá trị hợp lệ (true) hoặc không hợp lệ (false)
 */
function kiemTraRong(value, selectorError, name) {
  if (value === "" || value == 0) {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống!";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
function kiemTraDoDai(value, selectorError, name, minValue, maxValue) {
  if (value.length < minValue || value.length > maxValue) {
    document.querySelector(selectorError).innerHTML =
      name + " tối đa từ " + minValue + " - " + maxValue + " ký số!";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraTatCaKyTu(value, selectorError, name) {
  var regexLetter = /^[a-z A-Z]+$/; //Nhập các kí tự a-z A-Z hoặc khoảng trống không bao gồm unicode
  if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " phải là chữ!";
  return false;
}

function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " không đúng định dạng!";
  return false;
}

// function kiemTraMatKhau(value, selectorError, name) {
//   var regexPassword =
//     /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/;
//   if (regexPassword.test(value)) {
//     document.querySelector(selectorError).innerHTML = "";
//   }
//   document.querySelector(selectorError).innerHTML =
//     name +
//     " từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
// }

function kiemTraRangeLuong(value, selectorError, name, minRange, maxRange) {
  if (value < minRange || value > maxRange) {
    document.querySelector(selectorError).innerHTML =
      name + " từ " + minRange + " - " + maxRange;
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraChucVu(value, selectorError, name) {
  if (value === "Chọn chức vụ") {
    document.querySelector(selectorError).innerHTML = name + " không hợp lệ";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraSoGioLam(value, selectorError, name, minTime, maxTime) {
  if (value < minTime || value > maxTime) {
    document.querySelector(selectorError).innerHTML =
      name + " từ " + minTime + " - " + maxTime + "giờ";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraTrungTaiKhoan(value, selectorError, name) {
  viTri = timKiemViTri(mangNhanVien, value);
  if (viTri == -1) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " bị trùng";
}
