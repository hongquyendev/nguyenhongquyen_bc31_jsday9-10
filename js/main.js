var NhanVien = function (
  _tk,
  _ten,
  _email,
  _mk,
  _ngaylam,
  _luongCB,
  _chucvu,
  _giolam
) {
  this.tk = _tk;
  this.ten = _ten;
  this.email = _email;
  this.mk = _mk;
  this.ngaylam = _ngaylam;
  this.luongCB = _luongCB;
  this.chucvu = _chucvu;
  this.giolam = _giolam;
  this.tongluong = "";
};

var layThongTinTuForm = function () {
  var tk = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var mk = document.getElementById("password").value;
  var ngaylam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucvu = document.getElementById("chucvu").value;
  var giolam = document.getElementById("gioLam").value * 1;

  var nhanVien = new NhanVien(
    tk,
    ten,
    email,
    mk,
    ngaylam,
    luongCB,
    chucvu,
    giolam
  );
  return nhanVien;
};

function renderNhanVien(arrNhanVien) {
  var html = "";
  for (var i = 0; i < arrNhanVien.length; i++) {
    var nv = arrNhanVien[i];
    html += `
  <tr>
  <td>${nv.tk}</td>
  <td>${nv.ten}</td>
  <td>${nv.email}</td>
  <td>${nv.ngaylam}</td>
  <td>${nv.chucvu}</td>
  <td>${nv.tongluong}</td>
  </tr>
  `;
  }
  return html;
}

function luuLocalStorage(mangNhanVien) {
  var mangNhanVienJSON = JSON.stringify(mangNhanVien);
  localStorage.setItem("mangNhanVien", mangNhanVienJSON);
}

var mangNhanVien = [];
document.getElementById("btnThemNV").onclick = function () {
  var nv = layThongTinTuForm();
  console.log("nv: ", nv);
  mangNhanVien.push(layThongTinTuForm());
  luuLocalStorage(mangNhanVien);
  var html = renderNhanVien(mangNhanVien);
  document.querySelector("#tableDanhSach").innerHTML = html;
};
