var layThongTinTuForm = function () {
  var tk = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var mk = document.getElementById("password").value;
  var ngaylam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucvu = document.getElementById("chucvu").value;
  var giolam = document.getElementById("gioLam").value * 1;

  var nhanVien = new NhanVien(
    tk,
    ten,
    email,
    mk,
    ngaylam,
    luongCB,
    chucvu,
    giolam
  );
  return nhanVien;
};

function renderNhanVien(arrNhanVien) {
  var html = "";
  for (var i = 0; i < arrNhanVien.length; i++) {
    var nv = arrNhanVien[i];
    html += `
  <tr>
  <td>${nv.tk}</td>
  <td>${nv.ten}</td>
  <td>${nv.email}</td>
  <td>${nv.ngaylam}</td>
  <td>${nv.chucvu}</td>
  <td>${nv.tinhTongLuong()}</td>
  <td>${nv.xepLoaiNhanVien()}</td>
  <td><button class = 'btn btn-danger' onclick = "xoaNhanVien('${
    nv.tk
  }')">Xóa</button>
  <button class = 'btn btn-warning' onclick = "suaNhanVien('${
    nv.tk
  }')" data-toggle="modal" data-target="#myModal">Sửa</button>
  </td>
  </tr>
  `;
  }
  return (document.querySelector("#tableDanhSach").innerHTML = html);
}

/**
 *
 * @param {*} array Mảng cần duyệt tìm Index
 * @param {*} value Giá trị đặc trưng trong mảng của mỗi thành phần
 * @returns trả về giá trị Index
 */
function timKiemViTri(array, value) {
  return array.findIndex(function (item) {
    return item.tk == value;
  });
}
// function timKiemViTri(array, value) {
//   return array.findIndex((item) => item.tk == value);
// }

//-----------------ES6-----------------------
// const timKiemViTri = (array, value) =>
//   array.findIndex((item) => item.tk == value);

function hienThiThongTin(arrNhanVien) {
  document.getElementById("tknv").value = arrNhanVien.tk;
  document.getElementById("name").value = arrNhanVien.ten;
  document.getElementById("email").value = arrNhanVien.email;
  document.getElementById("password").value = arrNhanVien.mk;
  document.getElementById("datepicker").value = arrNhanVien.ngaylam;
  document.getElementById("luongCB").value = arrNhanVien.luongCB;
  document.getElementById("chucvu").value = arrNhanVien.chucvu;
  document.getElementById("gioLam").value = arrNhanVien.giolam;
}

function resetThongTin() {
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("datepicker").value;
  document.getElementById("luongCB").value = "";
  document.getElementById("chucvu").value = "Chọn chức vụ";
  document.getElementById("gioLam").value = "";
}
