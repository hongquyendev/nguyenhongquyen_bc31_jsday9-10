var mangNhanVien = [];
document.getElementById("btnThemNV").onclick = function () {
  var nv = layThongTinTuForm();

  // Kiểm tra dữ liệu trước khi thêm vào mảng
  var valid = true;

  //------------- Kiểm tra Tài khoản -------------
  if (kiemTraRong(nv.tk, "#tbTKNV", "Tài khoản")) {
    if (kiemTraDoDai(nv.tk, "#tbTKNV", "Tài khoản", 4, 6)) {
      valid &= kiemTraTrungTaiKhoan(nv.tk, "#tbTKNV", "Tài khoản");
    }
  }

  //------------- Kiểm tra Tên nhân viên -------------
  if (kiemTraRong(nv.ten, "#tbTen", "Tên nhân viên")) {
    valid &= kiemTraTatCaKyTu(nv.ten, "#tbTen", "Tên nhân viên");
  }

  //------------- Kiểm tra Email -------------
  if (kiemTraRong(nv.email, "#tbEmail", "Email")) {
    valid &= kiemTraEmail(nv.email, "#tbEmail", "Email");
  }

  //------------- Kiểm tra Mật khẩu -------------
  valid &= kiemTraRong(nv.mk, "#tbMatKhau", "Mật khẩu");
  // valid &= kiemTraMatKhau(nv.mk, "#tbMatKhau", "Mật khẩu");

  //------------- Kiểm tra Lương -------------
  if (kiemTraRong(nv.luongCB, "#tbLuongCB", "Lương cơ bản")) {
    valid &= kiemTraRangeLuong(
      nv.luongCB,
      "#tbLuongCB",
      "Lương cơ bản",
      1000000,
      20000000
    );
  }

  //------------- Kiểm tra Chức vụ -------------
  valid &= kiemTraChucVu(nv.chucvu, "#tbChucVu", "Chức vụ");

  //------------- Kiểm tra Giờ làm -------------
  valid &= kiemTraSoGioLam(nv.giolam, "#tbGiolam", "Giờ làm", 80, 200);

  // if (!valid) {
  //   return;
  // }

  mangNhanVien.push(nv);

  luuLocalStorage(mangNhanVien);

  renderNhanVien(mangNhanVien);

  resetThongTin();

  document.getElementById("btnThemNV").setAttribute("data-dismiss", "modal");
};

//Hàm sẽ chạy khi giao diện vừa load lên
window.onload = function () {
  var display = document.querySelectorAll(".sp-thongbao");
  [...display].forEach(function (item) {
    item.style.display = "block";
  });

  var content = layLocalStorage("mangNhanVien");

  if (content) {
    for (i = 0; i < content.length; i++) {
      var item = content[i];
      var nhanVien = new NhanVien(
        item.tk,
        item.ten,
        item.email,
        item.mk,
        item.ngaylam,
        item.luongCB,
        item.chucvu,
        item.giolam
      );
      mangNhanVien.push(nhanVien);
    }

    renderNhanVien(mangNhanVien);
  }
};

function luuLocalStorage(arrNhanVien) {
  var mangNhanVienJSON = JSON.stringify(arrNhanVien);
  localStorage.setItem("mangNhanVien", mangNhanVienJSON);
}

function layLocalStorage(arrNhanVien) {
  var output;
  if (localStorage.getItem(arrNhanVien)) {
    output = JSON.parse(localStorage.getItem(arrNhanVien));
  }
  return output;
}

function xoaNhanVien(taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);

  mangNhanVien.splice(viTri, 1);
  luuLocalStorage(mangNhanVien);
  renderNhanVien(mangNhanVien);
}

function suaNhanVien(taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);
  if (viTri !== -1) {
    var nv = mangNhanVien[viTri];
    document.getElementById("tknv").disabled = true;
    hienThiThongTin(nv);
  }
}
document.getElementById("btnCapNhat").onclick = () => {
  var nv = layThongTinTuForm(mangNhanVien);

  var viTri = timKiemViTri(mangNhanVien, nv.tk);

  // Kiểm tra dữ liệu trước khi thêm vào mảng
  var valid = true;

  //------------- Kiểm tra Tài khoản -------------
  if (kiemTraRong(nv.tk, "#tbTKNV", "Tài khoản")) {
    if (kiemTraDoDai(nv.tk, "#tbTKNV", "Tài khoản", 4, 6)) {
      // valid &= kiemTraTrungTaiKhoan(nv.tk, "#tbTKNV", "Tài khoản");
    }
  }

  //------------- Kiểm tra Tên nhân viên -------------
  if (kiemTraRong(nv.ten, "#tbTen", "Tên nhân viên")) {
    valid &= kiemTraTatCaKyTu(nv.ten, "#tbTen", "Tên nhân viên");
  }

  //------------- Kiểm tra Email -------------
  if (kiemTraRong(nv.email, "#tbEmail", "Email")) {
    valid &= kiemTraEmail(nv.email, "#tbEmail", "Email");
  }

  //------------- Kiểm tra Mật khẩu -------------
  valid &= kiemTraRong(nv.mk, "#tbMatKhau", "Mật khẩu");
  // valid &= kiemTraMatKhau(nv.mk, "#tbMatKhau", "Mật khẩu");

  //------------- Kiểm tra Lương -------------
  if (kiemTraRong(nv.luongCB, "#tbLuongCB", "Lương cơ bản")) {
    valid &= kiemTraRangeLuong(
      nv.luongCB,
      "#tbLuongCB",
      "Lương cơ bản",
      1000000,
      20000000
    );
  }

  //------------- Kiểm tra Chức vụ -------------
  valid &= kiemTraChucVu(nv.chucvu, "#tbChucVu", "Chức vụ");

  //------------- Kiểm tra Giờ làm -------------
  valid &= kiemTraSoGioLam(nv.giolam, "#tbGiolam", "Giờ làm", 80, 200);

  // if (!valid) {
  //   return;
  // }

  document.getElementById("tknv").disabled = false;

  mangNhanVien[viTri] = nv;

  luuLocalStorage(mangNhanVien);

  renderNhanVien(mangNhanVien);

  resetThongTin();

  document.getElementById("btnCapNhat").setAttribute("data-dismiss", "modal");
};

document.getElementById("btnTimNV").onclick = () => {
  var sortMangNhanVien = [];

  var timNhanVien = document.getElementById("searchName").value;

  if (timNhanVien) {
    for (let index = 0; index < mangNhanVien.length; index++) {
      const nv = mangNhanVien[index];

      if (timNhanVien == nv.xepLoaiNhanVien()) {
        var viTri = timKiemViTri(mangNhanVien, nv.tk);

        var sort = mangNhanVien[viTri];

        sortMangNhanVien.push(sort);
      }
    }

    renderNhanVien(sortMangNhanVien);
  } else {
    renderNhanVien(mangNhanVien);
  }
};
