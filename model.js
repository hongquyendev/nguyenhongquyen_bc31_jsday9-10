var NhanVien = function (
  _tk,
  _ten,
  _email,
  _mk,
  _ngaylam,
  _luongCB,
  _chucvu,
  _giolam
) {
  this.tk = _tk;
  this.ten = _ten;
  this.email = _email;
  this.mk = _mk;
  this.ngaylam = _ngaylam;
  this.luongCB = _luongCB;
  this.chucvu = _chucvu;
  this.giolam = _giolam;

  this.tinhTongLuong = function () {
    var tongLuong;
    if (this.chucvu === "Sếp") {
      tongLuong = this.luongCB * 3;
    } else {
      if (this.chucvu === "Trưởng phòng") {
        tongLuong = this.luongCB * 2;
      } else {
        if (this.chucvu === "Nhân viên") {
          tongLuong = this.luongCB;
        }
      }
    }
    return tongLuong;
  };

  this.xepLoaiNhanVien = function () {
    if (this.giolam >= 192) {
      return "Nhân viên xuất sắc";
    } else {
      if (this.giolam >= 176) {
        return "Nhân viên giỏi";
      } else {
        if (this.giolam >= 160) {
          return "Nhân viên khá";
        } else {
          return "Nhân viên trung bình";
        }
      }
    }
  };
};
